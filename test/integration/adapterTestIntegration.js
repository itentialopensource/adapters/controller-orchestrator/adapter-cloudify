/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cloudify',
      type: 'Cloudify',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Cloudify = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Cloudify Adapter Test', () => {
  describe('Cloudify Class Tests', () => {
    const a = new Cloudify(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cloudify-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cloudify-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listAgents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAgents(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Agents', 'listAgents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBlueprints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBlueprints(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blueprints', 'getBlueprints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const blueprintsBlueprintId = 'hello-world';
    describe('#updateBlueprint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBlueprint(null, null, null, blueprintsBlueprintId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(blueprintsBlueprintId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blueprints', 'updateBlueprint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setGlobalBlueprint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setGlobalBlueprint(blueprintsBlueprintId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(blueprintsBlueprintId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blueprints', 'setGlobalBlueprint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const blueprintsSetBlueprintVisibilityBodyParam = {};
    describe('#setBlueprintVisibility - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setBlueprintVisibility(blueprintsBlueprintId, blueprintsSetBlueprintVisibilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(blueprintsBlueprintId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blueprints', 'setBlueprintVisibility', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#summarizeBluepriints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.summarizeBluepriints(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Blueprints', 'summarizeBluepriints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listClusterNodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listClusterNodes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Clusters', 'listClusterNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const clustersHostname = 'node1.openstack.local';
    describe('#getClusterNode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getClusterNode(clustersHostname, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.hostname);
                assert.notEqual(undefined, data.response.hostname);
                assert.equal(clustersHostname, data.response.hostname);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Clusters', 'getClusterNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsDeploymentId = 'deployment_1';
    const deploymentsSetDeploymentSiteBodyParam = {};
    describe('#setDeploymentSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDeploymentSite(deploymentsDeploymentId, deploymentsSetDeploymentSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(deploymentsDeploymentId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'setDeploymentSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDeploymentUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDeploymentUpdate(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'listDeploymentUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeploymentUpdate(null, deploymentsDeploymentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.deployment_id);
                assert.notEqual(undefined, data.response.deployment_id);
                assert.equal(deploymentsDeploymentId, data.response.deployment_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'getDeploymentUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsUpdateDeploymentBodyParam = {};
    describe('#updateDeployment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeployment(deploymentsDeploymentId, deploymentsUpdateDeploymentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.deployment_id);
                assert.notEqual(undefined, data.response.deployment_id);
                assert.equal(deploymentsDeploymentId, data.response.deployment_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'updateDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeployments(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'getDeployments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsCreateDeploymentBodyParam = {};
    describe('#createDeployment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDeployment(null, deploymentsDeploymentId, deploymentsCreateDeploymentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(deploymentsDeploymentId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'createDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentCapabilities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeploymentCapabilities(deploymentsDeploymentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.deployment_id);
                assert.notEqual(undefined, data.response.deployment_id);
                assert.equal(deploymentsDeploymentId, data.response.deployment_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'getDeploymentCapabilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentOutputs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeploymentOutputs(deploymentsDeploymentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.deployment_id);
                assert.notEqual(undefined, data.response.deployment_id);
                assert.equal(deploymentsDeploymentId, data.response.deployment_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'getDeploymentOutputs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsSetDeploymentVisibilityBodyParam = {};
    describe('#setDeploymentVisibility - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDeploymentVisibility(deploymentsDeploymentId, deploymentsSetDeploymentVisibilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(deploymentsDeploymentId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'setDeploymentVisibility', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#summarizeDeployments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.summarizeDeployments(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'summarizeDeployments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listEvents((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'listEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const executionsExecutionId = 'ca3d7413-c8af-41a3-b864-571cef25899b';
    const executionsStartExecutionBodyParam = {};
    describe('#startExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startExecution(null, executionsStartExecutionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(executionsExecutionId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Executions', 'startExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const executionsCancelExecutionBodyParam = {};
    describe('#cancelExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelExecution(null, executionsExecutionId, executionsCancelExecutionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(executionsExecutionId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Executions', 'cancelExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExecutions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listExecutions(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Executions', 'listExecutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const executionsUpdateExecutionBodyParam = {};
    describe('#updateExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExecution(null, executionsExecutionId, executionsUpdateExecutionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(executionsExecutionId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Executions', 'updateExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExecution(null, executionsExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(executionsExecutionId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Executions', 'getExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#summarizeExecutions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.summarizeExecutions(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Executions', 'summarizeExecutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licenseUpdateLicenseBodyParam = {};
    describe('#updateLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateLicense(licenseUpdateLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.expiration_date);
                assert.notEqual(undefined, data.response.expiration_date);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'updateLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listLicense((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'listLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#status - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.status((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.status);
                assert.notEqual(undefined, data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Manager', 'status', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#version - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.version(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.version);
                assert.notEqual(undefined, data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Manager', 'version', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNodeInstances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listNodeInstances(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeInstances', 'listNodeInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeInstancesNodeInstanceId = 'http_web_server_tfq3nt';
    const nodeInstancesUpdateNodeInstanceBodyParam = {};
    describe('#updateNodeInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNodeInstance(null, nodeInstancesNodeInstanceId, nodeInstancesUpdateNodeInstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(nodeInstancesNodeInstanceId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeInstances', 'updateNodeInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNodeInstance(null, nodeInstancesNodeInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(nodeInstancesNodeInstanceId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeInstances', 'getNodeInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#summarizeNodeInstances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.summarizeNodeInstances(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeInstances', 'summarizeNodeInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listNodes(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'listNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#summarizeNodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.summarizeNodes(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'summarizeNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOperations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listOperations(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(0, data.response.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'listOperations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationsOperationId = 'operation-id';
    const operationsCreateOperationBodyParam = {};
    describe('#createOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOperation(operationsOperationId, operationsCreateOperationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(operationsOperationId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'createOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationsUpdateOperationBodyParam = {};
    describe('#updateOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOperation(operationsUpdateOperationBodyParam, operationsOperationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(operationsOperationId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'updateOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsPluginId = 'ecea687a-b7dc-4d02-909d-0400aa23df27';
    describe('#updatePlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePlugin(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(pluginsPluginId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPlugins - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPlugins(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'listPlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPluginsUpdates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPluginsUpdates(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'listPluginsUpdates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsBlueprintId = 'fakedata';
    const pluginsUpdatePluginsBodyParam = {};
    describe('#updatePlugins - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePlugins(pluginsBlueprintId, pluginsUpdatePluginsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(pluginsPluginId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPluginsUpdate(null, pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(pluginsPluginId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getPluginsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlugin(null, pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(pluginsPluginId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setGlobalPlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setGlobalPlugin(pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(pluginsPluginId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'setGlobalPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsSetPluginVisibilityBodyParam = {};
    describe('#setPluginVisibility - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setPluginVisibility(pluginsPluginId, pluginsSetPluginVisibilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(pluginsPluginId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'setPluginVisibility', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsImportSecretBodyParam = {};
    describe('#importSecrets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importSecrets(secretsImportSecretBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Secrets', 'importSecrets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSecrets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSecrets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'listSecrets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportSecrets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportSecrets(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(0, data.response.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'exportSecrets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsSecretKey = 'new_secret_key';
    const secretsCreateSecretBodyParam = {};
    describe('#createSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSecret(secretsSecretKey, secretsCreateSecretBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.key);
                assert.notEqual(undefined, data.response.key);
                assert.equal(secretsSecretKey, data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'createSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsUpdateSecretBodyParam = {};
    describe('#updateSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSecret(secretsSecretKey, secretsUpdateSecretBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.key);
                assert.notEqual(undefined, data.response.key);
                assert.equal(secretsSecretKey, data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'updateSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSecret(secretsSecretKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.key);
                assert.notEqual(undefined, data.response.key);
                assert.equal(secretsSecretKey, data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'getSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setGlobalSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setGlobalSecret(secretsSecretKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.key);
                assert.notEqual(undefined, data.response.key);
                assert.equal(secretsSecretKey, data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'setGlobalSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsSetSecretVisibilityBodyParam = {};
    describe('#setSecretVisibility - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSecretVisibility(secretsSecretKey, secretsSetSecretVisibilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.key);
                assert.notEqual(undefined, data.response.key);
                assert.equal(secretsSecretKey, data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'setSecretVisibility', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitesSiteName = 'Tel-Aviv';
    const sitesUpdateSiteBodyParam = {};
    describe('#updateSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSite(sitesSiteName, sitesUpdateSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(sitesSiteName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'updateSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSites - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSites((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'listSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitesCreateSiteBodyParam = {};
    describe('#createSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSite(sitesSiteName, sitesCreateSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(sitesSiteName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'createSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSite(sitesSiteName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(sitesSiteName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotsSnapshotId = 'bb9cd6df-7acd-4649-9fc1-fe062759cde8';
    const snapshotsRestoreSnapshotBodyParam = {};
    describe('#restoreSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreSnapshot(snapshotsSnapshotId, snapshotsRestoreSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(snapshotsSnapshotId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'restoreSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#snapshotRestoreStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotRestoreStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.status);
                assert.notEqual(undefined, data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'snapshotRestoreStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSnapshots - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSnapshots(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'listSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadSnapshot(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(snapshotsSnapshotId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'uploadSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(snapshotsSnapshotId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'createSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskGraphId = 'aaabbbccc';
    const taskGraphsCreateTaskGraphBodyParam = {};
    describe('#createTaskGraph - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTaskGraph(taskGraphsCreateTaskGraphBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(taskGraphId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskGraphs', 'createTaskGraph', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTaskGraphs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTaskGraphs(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.notEqual(undefined, data.response.id);
                assert.equal(taskGraphId, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskGraphs', 'listTaskGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsTenantName = 'tenant_name';
    describe('#createTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTenant(tenantsTenantName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(tenantsTenantName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'createTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTenants - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTenants((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'listTenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsAddUserGroupToTenantBodyParam = {};
    describe('#addUserGroupToTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUserGroupToTenant(tenantsAddUserGroupToTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(tenantsTenantName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'addUserGroupToTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsUpdateUserGroupInTenantBodyParam = {};
    describe('#updateUserGroupInTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserGroupInTenant(tenantsUpdateUserGroupInTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(tenantsTenantName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'updateUserGroupInTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsAddUserToTenantBodyParam = {};
    describe('#addUserToTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUserToTenant(tenantsAddUserToTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(tenantsTenantName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'addUserToTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsUpdateUserInTenantBodyParam = {};
    describe('#updateUserInTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserInTenant(tenantsUpdateUserInTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(tenantsTenantName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'updateUserInTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTenant(tenantsTenantName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(tenantsTenantName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'getTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsGroupName = 'group_name';
    const userGroupsCreateUserGroupBodyParam = {};
    describe('#createUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUserGroup(userGroupsCreateUserGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(userGroupsGroupName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'createUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listUserGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'listUserGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsAddUserToUserGroupBodyParam = {};
    describe('#addUserToUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUserToUserGroup(userGroupsAddUserToUserGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(userGroupsGroupName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'addUserToUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserGroup(userGroupsGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.name);
                assert.notEqual(undefined, data.response.name);
                assert.equal(userGroupsGroupName, data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'getUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsername = 'username';
    const usersUpdateUserBodyParam = {};
    describe('#updateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUser(usersUpdateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.username);
                assert.notEqual(undefined, data.response.username);
                assert.equal(usersUsername, data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersCreateUserBodyParam = {};
    describe('#createUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUser(usersCreateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.username);
                assert.notEqual(undefined, data.response.username);
                assert.equal(usersUsername, data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'createUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.items);
                assert.notEqual(undefined, data.response.items);
                assert.notEqual(0, data.response.items.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'listUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUser(usersUsername, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.username);
                assert.notEqual(undefined, data.response.username);
                assert.equal(usersUsername, data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBlueprint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBlueprint(null, blueprintsBlueprintId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Blueprints', 'deleteBlueprint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClusterNode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteClusterNode(clustersHostname, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Clusters', 'deleteClusterNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeployment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeployment(null, null, deploymentsDeploymentId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Deployments', 'deleteDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOperation(operationsOperationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Operations', 'deleteOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsDeletePluginBodyParam = {};
    describe('#deletePlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePlugin(null, pluginsPluginId, pluginsDeletePluginBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Plugins', 'deletePlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecret(secretsSecretKey, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Secrets', 'deleteSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSite(sitesSiteName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Sites', 'deleteSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Snapshots', 'deleteSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsRemoveuserGroupFromTenantBodyParam = {};
    describe('#removeUserGroupFromTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeUserGroupFromTenant(tenantsRemoveuserGroupFromTenantBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Tenants', 'removeUserGroupFromTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsRemoveUserFromTenantBodyParam = {};
    describe('#removeUserFromTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeUserFromTenant(tenantsRemoveUserFromTenantBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Tenants', 'removeUserFromTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTenant(tenantsTenantName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Tenants', 'deleteTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsRemoveUserFromUserGroupBodyParam = {};
    describe('#removeUserFromUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeUserFromUserGroup(userGroupsRemoveUserFromUserGroupBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('UserGroups', 'removeUserFromUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserGroup(userGroupsGroupName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('UserGroups', 'deleteUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser(usersUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              saveMockData('Users', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
