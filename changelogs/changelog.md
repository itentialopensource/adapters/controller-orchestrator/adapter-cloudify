
## 0.2.1 [05-14-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cloudify!5

---

## 0.2.0 [05-14-2022]

- Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
- Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into - various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
- Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
- Add scripts for easier authentication, removing hooks, etc
- Script changes (install script as well as database changes in other scripts)
- Double # of path vars on generic call
- Update versions of foundation (e.g. adapter-utils)
- Update npm publish so it supports https
- Update dependencies
- Add preinstall for minimist
- Fix new lint issues that came from eslint dependency change
- Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
- Add the adapter type in the package.json
- Add AdapterInfo.js script
- Add json-query dependency
- Add the propertiesDecorators.json for product encryption
- Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
- Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
- Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
- Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
- Add AdapterInfo.json
- Add systemName for documentation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cloudify!4

---

## 0.1.5 [03-03-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cloudify!3

---

## 0.1.4 [07-07-2020] & 0.1.3 [01-29-2020]

- Added the tenant information into Sample Properties so that it is easier for future users to configure.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cloudify!2

---

## 0.1.2 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cloudify!1

---

## 0.1.1 [11-19-2019]

- Initial Commit

See commit 552a078

---
