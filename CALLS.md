## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cloudify. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cloudify.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cloudify. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listAgents(nodeId, callback)</td>
    <td style="padding:15px">listAgents</td>
    <td style="padding:15px">{base_path}/{version}/agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBlueprints(id, include, callback)</td>
    <td style="padding:15px">getBlueprints</td>
    <td style="padding:15px">{base_path}/{version}/blueprints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBlueprint(applicationFileName, visibility, blueprintArchiveUrl, blueprintId, callback)</td>
    <td style="padding:15px">updateBlueprint</td>
    <td style="padding:15px">{base_path}/{version}/blueprints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBlueprint(force, blueprintId, callback)</td>
    <td style="padding:15px">deleteBlueprint</td>
    <td style="padding:15px">{base_path}/{version}/blueprints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setGlobalBlueprint(blueprintId, callback)</td>
    <td style="padding:15px">setGlobalBlueprint</td>
    <td style="padding:15px">{base_path}/{version}/blueprints/{pathv1}/set-global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBlueprintVisibility(blueprintId, body, callback)</td>
    <td style="padding:15px">setBlueprintVisibility</td>
    <td style="padding:15px">{base_path}/{version}/blueprints/{pathv1}/set-visibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">summarizeBluepriints(targetField, allTenants, callback)</td>
    <td style="padding:15px">summarizeBluepriints</td>
    <td style="padding:15px">{base_path}/{version}/summary/blueprints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listClusterNodes(callback)</td>
    <td style="padding:15px">listClusterNodes</td>
    <td style="padding:15px">{base_path}/{version}/managers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterNode(hostname, callback)</td>
    <td style="padding:15px">getClusterNode</td>
    <td style="padding:15px">{base_path}/{version}/managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClusterNode(hostname, callback)</td>
    <td style="padding:15px">deleteClusterNode</td>
    <td style="padding:15px">{base_path}/{version}/managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeployments(id, include, callback)</td>
    <td style="padding:15px">getDeployments</td>
    <td style="padding:15px">{base_path}/{version}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeployment(include, deploymentId, body, callback)</td>
    <td style="padding:15px">createDeployment</td>
    <td style="padding:15px">{base_path}/{version}/deployments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeployment(include, deleteLogs, deploymentId, callback)</td>
    <td style="padding:15px">deleteDeployment</td>
    <td style="padding:15px">{base_path}/{version}/deployments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDeploymentVisibility(deploymentId, body, callback)</td>
    <td style="padding:15px">setDeploymentVisibility</td>
    <td style="padding:15px">{base_path}/{version}/deployments/{pathv1}/set-visibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDeploymentSite(deploymentId, body, callback)</td>
    <td style="padding:15px">setDeploymentSite</td>
    <td style="padding:15px">{base_path}/{version}/deployments/{pathv1}/set-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeployment(deploymentId, body, callback)</td>
    <td style="padding:15px">updateDeployment</td>
    <td style="padding:15px">{base_path}/{version}/deployment-updates/{pathv1}/update/initiate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentUpdate(include, deploymentId, callback)</td>
    <td style="padding:15px">getDeploymentUpdate</td>
    <td style="padding:15px">{base_path}/{version}/deployment-updates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDeploymentUpdate(include, callback)</td>
    <td style="padding:15px">listDeploymentUpdate</td>
    <td style="padding:15px">{base_path}/{version}/deployment-updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentOutputs(deploymentId, callback)</td>
    <td style="padding:15px">getDeploymentOutputs</td>
    <td style="padding:15px">{base_path}/{version}/deployments/{pathv1}/outputs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentCapabilities(deploymentId, callback)</td>
    <td style="padding:15px">getDeploymentCapabilities</td>
    <td style="padding:15px">{base_path}/{version}/deployments/{pathv1}/capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">summarizeDeployments(targetField, callback)</td>
    <td style="padding:15px">summarizeDeployments</td>
    <td style="padding:15px">{base_path}/{version}/summary/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEvents(callback)</td>
    <td style="padding:15px">listEvents</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExecutions(include, callback)</td>
    <td style="padding:15px">listExecutions</td>
    <td style="padding:15px">{base_path}/{version}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startExecution(include, body, callback)</td>
    <td style="padding:15px">startExecution</td>
    <td style="padding:15px">{base_path}/{version}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExecution(include, executionId, callback)</td>
    <td style="padding:15px">getExecution</td>
    <td style="padding:15px">{base_path}/{version}/executions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelExecution(include, executionId, body, callback)</td>
    <td style="padding:15px">cancelExecution</td>
    <td style="padding:15px">{base_path}/{version}/executions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExecution(include, executionId, body, callback)</td>
    <td style="padding:15px">updateExecution</td>
    <td style="padding:15px">{base_path}/{version}/executions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">summarizeExecutions(targetField, callback)</td>
    <td style="padding:15px">summarizeExecutions</td>
    <td style="padding:15px">{base_path}/{version}/summary/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLicense(body, callback)</td>
    <td style="padding:15px">updateLicense</td>
    <td style="padding:15px">{base_path}/{version}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLicense(callback)</td>
    <td style="padding:15px">listLicense</td>
    <td style="padding:15px">{base_path}/{version}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">status(callback)</td>
    <td style="padding:15px">status</td>
    <td style="padding:15px">{base_path}/{version}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">version(include, callback)</td>
    <td style="padding:15px">version</td>
    <td style="padding:15px">{base_path}/{version}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNodeInstances(include, callback)</td>
    <td style="padding:15px">listNodeInstances</td>
    <td style="padding:15px">{base_path}/{version}/node-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeInstance(include, nodeInstanceId, callback)</td>
    <td style="padding:15px">getNodeInstance</td>
    <td style="padding:15px">{base_path}/{version}/node-instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNodeInstance(include, nodeInstanceId, body, callback)</td>
    <td style="padding:15px">updateNodeInstance</td>
    <td style="padding:15px">{base_path}/{version}/node-instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">summarizeNodeInstances(targetField, callback)</td>
    <td style="padding:15px">summarizeNodeInstances</td>
    <td style="padding:15px">{base_path}/{version}/summary/node_instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNodes(include, callback)</td>
    <td style="padding:15px">listNodes</td>
    <td style="padding:15px">{base_path}/{version}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">summarizeNodes(targetField, callback)</td>
    <td style="padding:15px">summarizeNodes</td>
    <td style="padding:15px">{base_path}/{version}/summary/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOperations(graphId, callback)</td>
    <td style="padding:15px">listOperations</td>
    <td style="padding:15px">{base_path}/{version}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOperation(operationId, body, callback)</td>
    <td style="padding:15px">createOperation</td>
    <td style="padding:15px">{base_path}/{version}/operations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOperation(body, operationId, callback)</td>
    <td style="padding:15px">updateOperation</td>
    <td style="padding:15px">{base_path}/{version}/operations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOperation(operationId, callback)</td>
    <td style="padding:15px">deleteOperation</td>
    <td style="padding:15px">{base_path}/{version}/operations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPlugins(include, callback)</td>
    <td style="padding:15px">listPlugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePlugin(pluginArchiveUrl, include, visibility, callback)</td>
    <td style="padding:15px">updatePlugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlugin(include, pluginId, callback)</td>
    <td style="padding:15px">getPlugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePlugin(include, pluginId, body, callback)</td>
    <td style="padding:15px">deletePlugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setGlobalPlugin(pluginId, callback)</td>
    <td style="padding:15px">setGlobalPlugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/set-global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPluginVisibility(pluginId, body, callback)</td>
    <td style="padding:15px">setPluginVisibility</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/set-visibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePlugins(blueprintId, body, callback)</td>
    <td style="padding:15px">updatePlugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins-updates/{pathv1}/update/initiate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsUpdate(include, pluginId, callback)</td>
    <td style="padding:15px">getPluginsUpdate</td>
    <td style="padding:15px">{base_path}/{version}/plugins-updates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPluginsUpdates(include, callback)</td>
    <td style="padding:15px">listPluginsUpdates</td>
    <td style="padding:15px">{base_path}/{version}/plugins-updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecrets(callback)</td>
    <td style="padding:15px">listSecrets</td>
    <td style="padding:15px">{base_path}/{version}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecret(secretKey, callback)</td>
    <td style="padding:15px">getSecret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecret(secretKey, body, callback)</td>
    <td style="padding:15px">updateSecret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecret(secretKey, body, callback)</td>
    <td style="padding:15px">createSecret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecret(secretKey, callback)</td>
    <td style="padding:15px">deleteSecret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setGlobalSecret(secretKey, callback)</td>
    <td style="padding:15px">setGlobalSecret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}/set-global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSecretVisibility(secretKey, body, callback)</td>
    <td style="padding:15px">setSecretVisibility</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}/set-visibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportSecrets(passphrase, nonEncrypted, callback)</td>
    <td style="padding:15px">exportSecrets</td>
    <td style="padding:15px">{base_path}/{version}/secrets/share/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importSecrets(body, callback)</td>
    <td style="padding:15px">importSecrets</td>
    <td style="padding:15px">{base_path}/{version}/secrets/share/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSites(callback)</td>
    <td style="padding:15px">listSites</td>
    <td style="padding:15px">{base_path}/{version}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSite(siteName, callback)</td>
    <td style="padding:15px">getSite</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSite(siteName, body, callback)</td>
    <td style="padding:15px">createSite</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSite(siteName, body, callback)</td>
    <td style="padding:15px">updateSite</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSite(siteName, callback)</td>
    <td style="padding:15px">deleteSite</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnapshots(include, callback)</td>
    <td style="padding:15px">listSnapshots</td>
    <td style="padding:15px">{base_path}/{version}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">createSnapshot</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">deleteSnapshot</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreSnapshot(snapshotId, body, callback)</td>
    <td style="padding:15px">restoreSnapshot</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadSnapshot(snapshotArchiveUrl, callback)</td>
    <td style="padding:15px">uploadSnapshot</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">snapshotRestoreStatus(callback)</td>
    <td style="padding:15px">snapshotRestoreStatus</td>
    <td style="padding:15px">{base_path}/{version}/snapshot-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTaskGraphs(executionId, name, callback)</td>
    <td style="padding:15px">listTaskGraphs</td>
    <td style="padding:15px">{base_path}/{version}/tasks_graphs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTaskGraph(body, callback)</td>
    <td style="padding:15px">createTaskGraph</td>
    <td style="padding:15px">{base_path}/{version}/tasks_graphs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTenants(callback)</td>
    <td style="padding:15px">listTenants</td>
    <td style="padding:15px">{base_path}/{version}/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(tenantName, callback)</td>
    <td style="padding:15px">getTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenant(tenantName, callback)</td>
    <td style="padding:15px">deleteTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTenant(tenantName, callback)</td>
    <td style="padding:15px">createTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToTenant(body, callback)</td>
    <td style="padding:15px">addUserToTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserInTenant(body, callback)</td>
    <td style="padding:15px">updateUserInTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserFromTenant(body, callback)</td>
    <td style="padding:15px">removeUserFromTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserGroupToTenant(body, callback)</td>
    <td style="padding:15px">addUserGroupToTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserGroupInTenant(body, callback)</td>
    <td style="padding:15px">updateUserGroupInTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserGroupFromTenant(body, callback)</td>
    <td style="padding:15px">removeUserGroupFromTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserGroups(callback)</td>
    <td style="padding:15px">listUserGroups</td>
    <td style="padding:15px">{base_path}/{version}/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserGroup(body, callback)</td>
    <td style="padding:15px">createUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserGroup(groupName, callback)</td>
    <td style="padding:15px">getUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserGroup(groupName, callback)</td>
    <td style="padding:15px">deleteUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToUserGroup(body, callback)</td>
    <td style="padding:15px">addUserToUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserFromUserGroup(body, callback)</td>
    <td style="padding:15px">removeUserFromUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">createUser</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsers(callback)</td>
    <td style="padding:15px">listUsers</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(username, callback)</td>
    <td style="padding:15px">getUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(username, callback)</td>
    <td style="padding:15px">deleteUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(body, callback)</td>
    <td style="padding:15px">updateUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
