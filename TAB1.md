# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Cloudify System. The API that was used to build the adapter for Cloudify is usually available in the report directory of this adapter. The adapter utilizes the Cloudify API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cloudify adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cloudify. 

With this adapter you have the ability to perform operations with Cloudify such as:

- Get Blueprint
- Deploy Blueprint

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
