# Cloudify

Vendor: Cloudify
Homepage: https://docs.cloudify.co/

Product: Cloudify
Product Page: https://docs.cloudify.co/

## Introduction
We classify Cloudify into the Data Center domain as Cloudify provides capabilities in automating the deployment, and management of applications and services on data center environment. We also classify Cloudify into the Network Services domain due to its role in orchestrating and automating network-related functionalities.

"Cloudify allows organizations an effortless transition to public cloud and Cloud-Native architecture by enabling them to automate their existing infrastructure alongside cloud native and distributed edge resources"

## Why Integrate
The Cloudify adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cloudify. 

With this adapter you have the ability to perform operations with Cloudify such as:

- Get Blueprint
- Deploy Blueprint

## Additional Product Documentation
The [API documents for Cloudify](https://docs.cloudify.co/api/v3/)